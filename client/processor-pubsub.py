# from threading import Thread
from google.cloud import pubsub_v1
from google.cloud import speech
from google.cloud import language
import pprint
import time
import json
import pickle

class AudioProcessor:
    def __init__(self):
        self.speech_client = speech.SpeechClient()
        self.lang_client = language.LanguageServiceClient()
        self.x_counter = 0
        
    def get_audio_to_text_operation(self,uri):
        audio=speech.types.RecognitionAudio(
                uri=uri
            )

        config=speech.types.RecognitionConfig(
                encoding='FLAC',
                language_code='en-IN'
            )
        response = self.speech_client.recognize(config, audio)
        return response
    
    def get_transcript_from_file_uri(self,uri):
#         bucket_address = "gs://samples-112/new_samples/converted/"
#         uri = bucket_address+uri
        response = self.get_audio_to_text_operation(uri)
    
        """
        results {
        alternatives {
        transcript: "specification the meeting this place I\'d rather be more affordable so you and make your career success"
        confidence: 0.9146217703819275
        }
        }
        results {
        alternatives {
        transcript: " I am"
        confidence: 0.4852932095527649
        }
        }
        results {
        alternatives {
        transcript: " useful apps are giving employee discount of 40% right so my supervisor another 40% suggest that pay you $80 in INR 6ad what your DP is only $16.76 every month for everybody how about that"
        confidence: 0.872408926486969
        }
        }
        results {
        alternatives {
        transcript: " Darshan craze how can I that it\'s over I\'m expecting your a pretrial until August 21st August 24th you will be charged only $16.76 ok so everythings ABCD"
        confidence: 0.9013625383377075
        }
        }
        """

        # response = operation.result(timeout=50)
        # transcript = []
        for result in response.results[0][1]:
            transcript.append(result.alternatives[0].transcript)
            print(result.alternatives[0].transcript)
        return transcript
    
    def analyze_text(self,transcript):
        document = language.types.Document(
            content=transcript,
            language='en',
            type='PLAIN_TEXT',
        )

        response = self.lang_client.analyze_sentiment(
            document=document,
            encoding_type='UTF32',
        )
        
        """
        
        sentences {
        text {
        content: "specification the meeting this place I\'d rather be more affordable so you and make your career success."
        }
        sentiment {
        magnitude: 0.699999988079071
        score: 0.699999988079071
        }
        }
        sentences {
        text {
        content: "I am"
        begin_offset: 105
        }
        sentiment {
        magnitude: 0.10000000149011612
        score: 0.10000000149011612
        }
        }
        document_sentiment {
        magnitude: 0.800000011920929
        score: 0.4000000059604645
        }
        language: "en"
        
        """
        
        data = [[],[]]
        for el in response.sentences:
            data[0].append([el.sentiment.score,el.sentiment.magnitude])
            data[1].append([el.text.content])
            self.x_counter+=1
        return data

def transcribe_gcs(file):
    client = speech.SpeechClient()
    langClient = language.LanguageServiceClient()

    audio = types.RecognitionAudio(uri=gcs_uri)
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.FLAC,
        language_code='en-IN')

    operation = client.long_running_recognize(config, audio)

    print('Waiting for operation to complete...')
    response = operation.result(timeout=90)

    # Print the first alternative of all the consecutive results.
    for result in response.results:
        print('Transcript: {}'.format(result.alternatives[0].transcript))
        print('Confidence: {}'.format(result.alternatives[0].confidence))
        document = typesL.Document(
        content=result.alternatives[0].transcript,
        type=enumsL.Document.Type.PLAIN_TEXT)
        sentiment = langClient.analyze_sentiment(document=document).document_sentiment
        print('Sentiment: {}, {}'.format(sentiment.score, sentiment.magnitude))

    """Asynchronously transcribes the audio file specified by the gcs_uri."""
    # bucket_uri = 'streaming-upload-audio'
    # print("starting")
    # # file = 'new_file_0.flac'
    # file_0 = file.split('.')[0]
    # file_uri = 'gs://'+bucket_uri+'/'+file
    # processor_obj = AudioProcessor()
    # transcript = processor_obj.get_transcript_from_file_uri(file_uri)
    # print("got transcript")
    # text = ". ".join(transcript)
    # response = processor_obj.analyze_text(text)
    # with open('/conv_data/'+file.split('.')[0]+'_'+str(i)+'.pickle', 'wb') as f:
    #     pickle.dump(response, f, pickle.HIGHEST_PROTOCOL)
    # print("done")


def receive_messages(project, subscription_name):
    """Receives messages from a pull subscription."""
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        project, subscription_name)

    def callback(message):
        print('Received message: {}'.format(message.data))
        
        transcribe_gcs("gs://streaming-upload-audio/"+json.loads(message.data.decode()).get('filename',''))
        # transcribe_gcs(json.loads(message.data.decode()).get('file_name',''))
        message.ack()

    subscriber.subscribe(subscription_path, callback=callback)

    # The subscriber is non-blocking, so we must keep the main thread from
    # exiting to allow it to process messages in the background.
    print('Listening for messages on {}'.format(subscription_path))
    while True:
        time.sleep(60)

if __name__ == '__main__':
    receive_messages("helloworld-166506","codegrind-dhruv")