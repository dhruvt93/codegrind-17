from flask import Flask, render_template,jsonify
from threading import Lock
from flask_socketio import SocketIO
from flask_socketio import send, emit, disconnect
import random
import json
import time
from audioprocessor import AudioProcessor
from math import ceil
from google.cloud import speech
from google.cloud import language
import pprint
import pickle

async_mode = None

app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()

def background_thread():
    # for i in range(5):
    print("background")

    socketio.emit('status',{'msg':'Starting audio processing'})
    responses = []
    files = ['cust_trimmed_000.pickle',
             'cust_trimmed_001.pickle',
             'cust_trimmed_002.pickle',
             'cust_trimmed_003.pickle',
             'cust_trimmed_004.pickle',
            ]
    for file in files:
        with open(file, 'rb') as f:
            response = pickle.load(f)
            responses.append(response)
    # pprint.pprint(response)
    for response in responses:
        socketio.emit('new_data',{'data':response})
        time.sleep(5)


    # file_uri_list = ['cust_trimmed_000.flac',]
    # processor_obj = AudioProcessor()
    # for file in file_uri_list:
    #     transcript = processor_obj.get_transcript_from_file_uri(file)
    #     socketio.emit('status',{'msg':'Got transcript'})
    #     num_lines = len(transcript)
    #     one_time = 30
    #     divides = ceil(num_lines/one_time)
    #     portion = num_lines/divides
    #     sentiment_data = {}
    #     for i in range(divides):
    #         start = ceil(i*portion)
    #         end = ceil(start+portion)
    #         # print(start,end)
    #         text = "".join(transcript[start:end])
    #         response = processor_obj.analyze_text(text)
    #         # print(type)
    #         socketio.emit('new_data',{'data':response})
    # disconnect()


        # socketio.emit('new_data',
        #      {'msg': 'background-iter-'+str(i)})
        # time.sleep(3)

@socketio.on('connect')
def test_connect():
    print("connected")
    # global thread
    # with thread_lock:
    #     if thread is None:
    #         thread = socketio.start_background_task(target=background_thread)

    # emit('my_response', {'data': 'Connected', 'count': 0})

@socketio.on('request_data')
def request_data():
    print("client requesting data")
    global thread
    with thread_lock:
        # socketio.start_background_task(target=background_thread)
        print(thread)
        if thread is None:
            thread = socketio.start_background_task(target=background_thread)

# @socketio.on('disconnect')
# def test_connect():
#     socketio.emit('response',
#               {'msg': "Disconnecting..."})
#     disconnect()

@app.route("/")
def index():
    global thread
    thread = None
    return render_template("lines_sock2.html")

@app.route("/refresh/<int:counter>")
def refresh(counter):
    arr = []
    mul=5
    start = counter*mul
    end = start+mul
    for i in range(start,end):
        arr.append({"x":i+1,"y":round(random.random(),2),"y0":0,"meta":{"key":"first"}})
    return jsonify(arr)

    
if __name__ == '__main__':
    socketio.run(app,debug=True)