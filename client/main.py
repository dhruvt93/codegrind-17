from bokeh.plotting import figure
from bokeh.embed import components
from flask import Flask, render_template

app = Flask(__name__)

def create_hover_tool():
    # we'll code this function in a moment
    return None


def create_chart(hover_tool):
    x = [1, 2, 3, 4, 5]
    y = [6, 7, 8, 7, 3]

    tools = []
    if hover_tool:
        tools = [hover_tool,]

    p = figure(plot_width=1280, plot_height=720, tools=tools)

    # add both a line and circles on the same plot
    p.line(x, y, line_width=2)
    p.circle(x, y, fill_color="white", size=8)
    return p


@app.route("/")
def chart():
    hover = create_hover_tool()
    plot = create_chart(hover)
    script, div = components(plot)

    return render_template("bokeh_demo.html",
                           the_div=div, the_script=script)

if __name__ == '__main__':
    app.run(debug=True)