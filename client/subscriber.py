import subprocess
from google.cloud import pubsub_v1
import time
import sys
import json

once_over = False
def subprocess_cmd(command):
    process = subprocess.Popen(command,stdout=subprocess.PIPE, shell=True)
    proc_stdout = process.communicate()[0].strip()
    print(proc_stdout)

def receive_messages(project, subscription_name):
    """Receives messages from a pull subscription."""
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        project, subscription_name)

    def callback(message):
        print('Received message: {}'.format(message))
        # print(message.data.decode())
        data = json.loads(message.data.decode())
        file_name = data.get('file_name','')
        # call_id = str(data.get('call_id',''))
        # file=message.data.decode('UTF-8').split(".")[0]
        # cmd = "curl http://localhost:5000/start-process?file_name="+file_name+"&call_id="+call_id
        # print(cmd)
        process_cmd = "python rt_pipeline.py --file_name "+file_name
        message.ack()
        print("acked")
        subprocess_cmd(process_cmd)
        print("processed")
        file_name_0 = file_name.split('.')[0]
        if once_over == False:
            send_to_server_cmd = "curl http://localhost:5000/fetch-data?file_name="+file_name_0
            print(send_to_server_cmd)
            subprocess_cmd(send_to_server_cmd)
            # once_over=True
        # sys.exit(0)


    subscriber.subscribe(subscription_path, callback=callback)

    # The subscriber is non-blocking, so we must keep the main thread from
    # exiting to allow it to process messages in the background.
    print('Listening for messages on {}'.format(subscription_path))
    while True:
        time.sleep(60)

if __name__ == '__main__':
    receive_messages("helloworld-166506","codegrind-dhruv")