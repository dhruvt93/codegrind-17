from threading import Thread
from google.cloud import pubsub_v1
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
from google.cloud import language
from google.cloud.language import enums as enumsL
from google.cloud.language import types as typesL
import time
import json

def transcribe_gcs(gcs_uri):
    """Asynchronously transcribes the audio file specified by the gcs_uri."""
    client = speech.SpeechClient()
    langClient = language.LanguageServiceClient()

    audio = types.RecognitionAudio(uri=gcs_uri)
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.FLAC,
        language_code='en-IN')

    operation = client.long_running_recognize(config, audio)

    print('Waiting for operation to complete...')
    response = operation.result(timeout=90)

    # Print the first alternative of all the consecutive results.
    for result in response.results:
        print('Transcript: {}'.format(result.alternatives[0].transcript))
        print('Confidence: {}'.format(result.alternatives[0].confidence))
        document = typesL.Document(
        content=result.alternatives[0].transcript,
        type=enumsL.Document.Type.PLAIN_TEXT)
        sentiment = langClient.analyze_sentiment(document=document).document_sentiment
        print('Sentiment: {}, {}'.format(sentiment.score, sentiment.magnitude))

def receive_messages(project, subscription_name):
    """Receives messages from a pull subscription."""
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        project, subscription_name)

    def callback(message):
        print('Received message: {}'.format(message.data))
        transcribe_gcs("gs://streaming-upload-audio/"+json.loads(message.data.decode()).get('filename',''))
        message.ack()

    subscriber.subscribe(subscription_path, callback=callback)

    # The subscriber is non-blocking, so we must keep the main thread from
    # exiting to allow it to process messages in the background.
    print('Listening for messages on {}'.format(subscription_path))
    while True:
        time.sleep(60)

if __name__ == '__main__':
    receive_messages("helloworld-166506","codegrind-dhruv")