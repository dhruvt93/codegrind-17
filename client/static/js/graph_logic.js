
// var fillDetail = function(data){
// 	var dataEl = $("#data");
// 	// dataEl.empty();
// 	dataEl.innerHTML = "<p>"+data+"</p>";
// };


var counter = 0;
var series = [{"x":0,"y":0,"y0":0}];
var posSeries = [{"x":0,"y":0,"y0":0}];
var negSeries = [{"x":0,"y":0,"y0":0}];

var graph = new Rickshaw.Graph( {
	element: document.getElementById("chart"),
	width: 960,
	height: 500,
	renderer: 'multi',
	min:'auto',
	padding:{
		top:0.1,
		right:0.1,
		left:0,
		bottom:0.1
	},
	series: [
		{
			color: "#36454f",
			data: series,
			name: 'Sentiment flow',
			strokeWidth: 4,
			opacity: 0.5,
			meta:'meta',
			renderer:'line'
		},
		{
			color: "#028900",
			data: posSeries,
			name: 'Positive points',
			// strokeWidth: 5,
			opacity: 0.8,
			meta:'meta',
			renderer:'scatterplot',
			r:8
		},
		{
			color: "#fe2e2e",
			data: negSeries,
			name: 'Negative points',
			// strokeWidth: 5,
			opacity: 0.8,
			meta:'meta',
			renderer:'scatterplot',
			r:8
		}
	]
} );


// var preview = new Rickshaw.Graph.RangeSlider.Preview( {
// 	graph: graph,
// 	element: document.getElementById('preview'),
// } );
// var x_axis = new Rickshaw.Graph.Axis.Time( { graph: graph } );
var x_axis = new Rickshaw.Graph.Axis.X( { graph: graph, pixelsPerTick: 200 } );

var y_axis = new Rickshaw.Graph.Axis.Y( {
        graph: graph,
        orientation: 'left',
        tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
        element: document.getElementById('y_axis'),
} );

graph.render();
graph.renderer.dotSize = 8;

// new Rickshaw.Graph.HoverDetail({ graph: graph });

var legend = document.querySelector('#legend');
// var Hover = Rickshaw.Class.create(Rickshaw.Graph.HoverDetail, {
// 	render: function(args) {
// 		console.log(args);
// 		legend.innerHTML = args.formattedXValue;
// 		args.detail.sort(function(a, b) { return a.order - b.order }).forEach( function(d) {
// 			// console.log(d);
// 			var line = document.createElement('div');
// 			line.className = 'line';
// 			var swatch = document.createElement('div');
// 			swatch.className = 'swatch';
// 			swatch.style.backgroundColor = d.series.color;
// 			var label = document.createElement('div');
// 			label.className = 'label';
// 			label.innerHTML = d.name + ": " + d.formattedYValue;
// 			line.appendChild(swatch);
// 			line.appendChild(label);
// 			legend.appendChild(line);
// 			var dot = document.createElement('div');
// 			dot.className = 'dot';
// 			dot.style.top = graph.y(d.value.y) + 'px';
// 			dot.style.borderColor = d.series.color;
// 			this.element.appendChild(dot);
// 			dot.className = 'dot active';
// 			this.show();
// 		}, this );
//         }
// });


// var Hover = Rickshaw.Class.create(Rickshaw.Graph.HoverDetail, {
// 	render: function(args) {
// 		console.log(args);
// 		legend.innerHTML = args.formattedXValue;
// 		var d = args.detail[0];
// 			// console.log(d);
// 			var line = document.createElement('div');
// 			line.className = 'line';
// 			var swatch = document.createElement('div');
// 			swatch.className = 'swatch';
// 			swatch.style.backgroundColor = d.series.color;
// 			var label = document.createElement('div');
// 			label.className = 'label';
// 			label.innerHTML = d.name + ": " + d.formattedYValue;
// 			line.appendChild(swatch);
// 			line.appendChild(label);
// 			legend.appendChild(line);
// 			var dot = document.createElement('div');
// 			dot.className = 'dot';
// 			dot.style.top = graph.y(d.value.y) + 'px';
// 			dot.style.borderColor = d.series.color;
// 			this.element.appendChild(dot);
// 			dot.className = 'dot active';
// 			this.show();

//         }
// });
// var hover = new Hover( { graph: graph } ); 


// var slider = new Rickshaw.Graph.RangeSlider({
//     graph: graph,
//     element: document.querySelector('#slider')
// });




var hoverDetail = new Rickshaw.Graph.HoverDetail( {
	graph: graph,
	formatter: function(series, x, y,name1,name2,name3) {
		// console.log(series);
		// if (series.name=="Positive points" || series.name=="Negative points"){
		// 	// console.log("True");
		// }
		// else{
		// 	return null;
		// }
			var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
			var content = swatch +"Sentiment score" + ": " + parseFloat(y).toFixed(3)+'<br>'
			+"Magnitude: "+parseFloat(name3.value.meta.magnitude).toFixed(3)+'<br>'
			+"Text"+": "+name3.value.meta.text;
			// console.log(content);
			// fillDetail(name3.value.meta.text);
			return content;
		// console.log(name3);
		// console.log(series);
		// var date = '<span class="date">' + new Date(x * 1000).toUTCString() + '</span>';

	}
} );

// var legend = new Rickshaw.Graph.Legend( {
// 	graph: graph,
// 	element: document.getElementById('legend')

// } );

// var shelving = new Rickshaw.Graph.Behavior.Series.Toggle( {
// 	graph: graph,
// 	legend: legend
// } );

var setupSocket = function(){
	var socket = io.connect('http://' + document.domain + ':' + location.port);
	console.log("connecting...");
    // socket.on('connect', function() {
    //     socket.emit('my event', {data: 'I\'m connected!'});
    // });

    socket.on('new_data',function(msg){
    	// console.log(msg);
    	result = msg.data

		// graph.series[0].data.shift();
		// graph.series[1].data.shift();
		// graph.series[2].data.shift();


		// splitting into pos and neg

		for(var i=0;i<result.length;i++){
			if(result[i].y>0){
				posSeries.push(result[i]);
			}
			else{
				negSeries.push(result[i]);
			}
		}

		if(graph.series[0].data.length>50){
			graph.series[0].data = graph.series[0].data.concat(result).slice(4);
			graph.series[1].data = graph.series[1].data.concat(posSeries).slice(4);
			graph.series[2].data = graph.series[2].data.concat(negSeries).slice(4);
		}
		else{
		graph.series[0].data = graph.series[0].data.concat(result);
		// .slice(4);
		graph.series[1].data = graph.series[1].data.concat(posSeries);
		graph.series[2].data = graph.series[2].data.concat(negSeries);
		// .slice(4);
			
		}


		// earlier logic
		// if(graph.series[0].data.length>50){
		// 	graph.series[0].data = graph.series[0].data.concat(result).slice(4);
		// 	graph.series[1].data = graph.series[1].data.concat(result).slice(4);
		// }
		// else{
		// graph.series[0].data = graph.series[0].data.concat(result);
		// // .slice(4);
		// graph.series[1].data = graph.series[1].data.concat(result);
		// // .slice(4);
			
		// }
		// graph.series[1].data.push.apply(graph.series[1].data,result);
		// graph.series[1].data = result;
		graph.update();

		// $("circle").hover(function(){
		// 	console.log("hover!");
		// });
		// console.log(graph.series[0].data.length);
		counter+=1;


	    });

    socket.on('status',function(msg){
	console.log(msg);
	});


	$("#get").click(function(){
	    $.get("/start-job", function(data, status){
	        console.log("Data: " + data + "\nStatus: " + status);
	    });
	});

	$("#stop").click(function(){
		console.log("disconnecting...");

	    // $.get("/disconnect-socket");
		socket.disconnect();
	});

};



$( document ).ready(function() {
	setupSocket();

	// $( document ).on('hover','circle',function(){
	// 	console.log("hover!");
	// });


});