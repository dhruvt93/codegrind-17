var countries_codes = ["AFG","AGO","ALB","ARE","ARG","ARM","ATA",
	"ATF","AUS","AUT","AZE","BDI","BEL","BEN","BFA","BGD","BGR","BHS",
	"BIH","BLR","BLZ","BOL","BRA","BRN","BTN","BWA","CAF","CAN","CHE",
	"CHL","CHN","CIV","CMR","COD","COG","COL","CRI","CUB","CYP",
	"CZE","DEU","DJI","DNK","DOM","DZA","ECU","EGY","ERI","ESP","EST",
	"ETH","FIN","FJI","FLK","FRA","GUF","GAB","GBR","GEO","GHA","GIN",
	"GMB","GNB","GNQ","GRC","GRL","GTM","GUY","HND","HRV","HTI","HUN",
	"IDN","IND","IRL","IRN","IRQ","ISL","ISR","ITA","JAM","JOR","JPN",
	"KAZ","KEN","KGZ","KHM","KOR","KWT","LAO","LBN","LBR","LBY",
	"LKA","LSO","LTU","LUX","LVA","MAR","MDA","MDG","MEX","MKD","MLI",
	"MMR","MNE","MNG","MOZ","MRT","MWI","MYS","NAM","NCL","NER","NGA",
	"NIC","NLD","NOR","NPL","NZL","OMN","PAK","PAN","PER","PHL","PNG",
	"POL","PRI","PRK","PRT","PRY","QAT","ROU","RUS","RWA","ESH","SAU",
	"SDN","SSD","SEN","SLB","SLE","SLV","SOM","SRB","SUR","SVK",
	"SVN","SWE","SWZ","SYR","TCD","TGO","THA","TJK","TKM","TLS","TTO",
	"TUN","TUR","TWN","TZA","UGA","UKR","URY","USA","UZB","VEN","VNM",
	"VUT","PSE","YEM","ZAF","ZMB","ZWE"];

var fills = {
          defaultFill: '#8DA0A0',
          green1:'#52bf90',    //1 is light, 5 is dark
          green2:'#49ab81',
          green3:'#419873',
          green4:'#398564',
          green5:'#317256',
          red1:'#fe8181',
          red2:'#fe5757',
          red3:'#fe2e2e',
          red4:'#cb2424',
          red5:'#b62020'
        };

var fill_codes = ['green1','green2','green3','green4','green5',
				'red1','red2','red3','red4','red5'];

var sentiment = null;

var code_to_sentiment = function(code){
	if (code=='green1')
	sentiment = (Math.random() * (0.2 - 0.0) + 0.0).toFixed(4);
	else if (code=='green2')
	sentiment = (Math.random() * (0.4 - 0.2) + 0.2).toFixed(4);
	else if (code=='green3')
	sentiment = (Math.random() * (0.6 - 0.4) + 0.4).toFixed(4);
	else if (code=='green4')
	sentiment = (Math.random() * (0.8 - 0.6) + 0.6).toFixed(4);
	else if (code=='green5')
	sentiment = (Math.random() * (1 - 0.8) + 0.8).toFixed(4);
	else if (code=='red1')
	sentiment = (Math.random() * (-0.2 - 0.0) - 0.0).toFixed(4);
	else if (code=='red2')
	sentiment = (Math.random() * (-0.4 + 0.2) - 0.2).toFixed(4);
	else if (code=='red3')
	sentiment = (Math.random() * (-0.6 + 0.4) - 0.4).toFixed(4);
	else if (code=='red4')
	sentiment = (Math.random() * (-0.8 + 0.6) - 0.6).toFixed(4);
	else if (code=='red5')
	sentiment = (Math.random() * (-1 + 0.8) - 0.8).toFixed(4);

	return sentiment;

};
// var mapData = {
//           USA: {fillKey: 'g1' },
//           RUS: {fillKey: 'g2' },
//           CAN: {fillKey: 'g1' },
//           BRA: {fillKey: 'r2' },
//           ARG: {fillKey: 'r1'},
//           COL: {fillKey: 'g3' },
//           AUS: {fillKey: 'r3' },
//           ZAF: {fillKey: 'r3' },
//           MAD: {fillKey: 'g1' }       
//         };
var mapData = {};

var random_fill = null;
// for(var i=0;i<countries_codes.length;i++){
// 	random_fill=fill_codes[Math.floor(Math.random() * fill_codes.length)];
// 	mapData[countries_codes[i]]={fillKey:random_fill};
// }

var map = new Datamap({
scope: 'world',
element: document.getElementById('map'),
responsive:true,
projection: 'mercator',
fills: fills,
data: mapData,
geographyConfig: {
            popupTemplate: function(geo, data) {
                return ['<div class="hoverinfo"><strong>',
                        'Number of things in ' + geo.properties.name,
                        ': ' + data.fillKey,
                        '</strong></div>'].join('');
            }
        }
});

$("#randomiseMap").click(function(){
	// console.log(map);
	for(var i=0;i<countries_codes.length;i++){
	random_fill=fill_codes[Math.floor(Math.random() * fill_codes.length)];
	mapData[countries_codes[i]]={fillKey:random_fill};
}
	map.updateChoropleth(mapData);
	// map.fills.data={
 //          USA: {fillKey: 'r1' },
 //          RUS: {fillKey: 'r2' },
 //          CAN: {fillKey: 'r1' },
 //          BRA: {fillKey: 'g2' },
 //          ARG: {fillKey: 'r1'},
 //          COL: {fillKey: 'r3' },
 //          AUS: {fillKey: 'g3' },
 //          ZAF: {fillKey: 'g3' },
 //          MAD: {fillKey: 'r1' }       
 //        };
});

 // var map = new Datamap({
 //        element: document.getElementById('map'),
 //        scope: 'usa'
 //    });

//  var bubble_map = new Datamap({
//             element: document.getElementById('map'),
//             scope: 'india',
//             geographyConfig: {
//                 popupOnHover: true,
//                 highlightOnHover: true,
//                 borderColor: '#444',
//                 borderWidth: 0.5,
//                 dataUrl: 'https://rawgit.com/Anujarya300/bubble_maps/master/data/geography-data/india.topo.json'
//                 //dataJson: topoJsonData
//             },
//             fills: {
//                 'MAJOR': '#306596',
//                 'MEDIUM': '#0fa0fa',
//                 'MINOR': '#bada55',
//                 defaultFill: '#dddddd'
//             },
//             data: {
//                 'JH': { fillKey: 'MINOR' },
//                 'MH': { fillKey: 'MINOR' }
//             },
//             setProjection: function (element) {
//                 var projection = d3.geo.mercator()
//                     .center([78.9629, 23.5937]) // always in [East Latitude, North Longitude]
//                     .scale(1000);
//                 var path = d3.geo.path().projection(projection);
//                 return { path: path, projection: projection };
//             }
// });