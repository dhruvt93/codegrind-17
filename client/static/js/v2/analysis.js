var random_int = function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var get_colors = function(length){
	var arr = [];
	for (var i=0;i<length;i++){
		arr.push(dynamicColors());
	}
	return arr;
}

var dynamicColors = function() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
}

var ctx = document.getElementById("myChart");


data = {
    datasets: [{
        data: [10],
        backgroundColor:[]
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: ['placeholder']
};

options = {'legend':
                {'position':'bottom'}
            }
var myPieChart = new Chart(ctx,{
    type: 'doughnut',
    data: data,
    options: options
});



// var countries_codes = ["AFG","AGO","ALB","ARE","ARG","ARM","ATA",
//     "ATF","AUS","AUT","AZE","BDI","BEL","BEN","BFA","BGD","BGR","BHS",
//     "BIH","BLR","BLZ","BOL","BRA","BRN","BTN","BWA","CAF","CAN","CHE",
//     "CHL","CHN","CIV","CMR","COD","COG","COL","CRI","CUB","CYP",
//     "CZE","DEU","DJI","DNK","DOM","DZA","ECU","EGY","ERI","ESP","EST",
//     "ETH","FIN","FJI","FLK","FRA","GUF","GAB","GBR","GEO","GHA","GIN",
//     "GMB","GNB","GNQ","GRC","GRL","GTM","GUY","HND","HRV","HTI","HUN",
//     "IDN","IND","IRL","IRN","IRQ","ISL","ISR","ITA","JAM","JOR","JPN",
//     "KAZ","KEN","KGZ","KHM","KOR","KWT","LAO","LBN","LBR","LBY",
//     "LKA","LSO","LTU","LUX","LVA","MAR","MDA","MDG","MEX","MKD","MLI",
//     "MMR","MNE","MNG","MOZ","MRT","MWI","MYS","NAM","NCL","NER","NGA",
//     "NIC","NLD","NOR","NPL","NZL","OMN","PAK","PAN","PER","PHL","PNG",
//     "POL","PRI","PRK","PRT","PRY","QAT","ROU","RUS","RWA","ESH","SAU",
//     "SDN","SSD","SEN","SLB","SLE","SLV","SOM","SRB","SUR","SVK",
//     "SVN","SWE","SWZ","SYR","TCD","TGO","THA","TJK","TKM","TLS","TTO",
//     "TUN","TUR","TWN","TZA","UGA","UKR","URY","USA","UZB","VEN","VNM",
//     "VUT","PSE","YEM","ZAF","ZMB","ZWE"];

var countries_codes = ["AFG","AGO","ALB","ARE","ARG","ARM","ATA",
    "ATF","AUS","BDI","BEL","BEN","BFA",
    "BIH","BLR","BLZ","BOL","BRA","BRN","BTN","BWA","CAF","CAN","CHE",
    "CHL","CHN","COG","COL","CRI","CUB","CYP",
    "CZE","DEU","DJI","DNK","DOM","ERI","ESP","EST",
    "ETH","FIN","FJI","FLK","FRA","GUF","GAB","GBR","GEO","GHA","GIN",
    "GMB","GNB","GNQ","GRC","GTM","GUY","HND","HRV","HTI","HUN",
    "IDN","IND","IRL","IRN","IRQ","ISL","ISR","ITA","JAM","JOR","JPN",
    "KAZ","KHM","KOR","KWT","LAO","LBN","LBR","LBY",
    "LKA","LSO","LTU","LUX","LVA","MAR","MDA","MDG","MEX","MKD","MLI",
    "MMR","MNE","MNG","MOZ","MRT","MWI","PER","PHL","PNG",
    "POL","PRI","PRK","PRT","PRY","QAT","ROU","RWA","ESH","SAU",
    "SDN","SSD","SEN",
    "SVN","SWE","SWZ","SYR","TCD","TGO","THA","TJK","TTO",
    "TUN","UKR","URY","USA","UZB","VEN","VNM",
    "VUT","ZAF","ZMB","ZWE"];

var fills = {
          defaultFill: '#8DA0A0',
          green1:'#52bf90',    //1 is light, 5 is dark
          green2:'#49ab81',
          green3:'#419873',
          green4:'#398564',
          green5:'#317256',
          red1:'#fe8181',
          red2:'#fe5757',
          red3:'#fe2e2e',
          red4:'#cb2424',
          red5:'#b62020'
        };
var fill_codes = ['green1','green2','green3','green4','green5',
                'red1','red2','red3','red4','red5'];



var sentiment = null;

var code_to_sentiment = function(code){
    if (code=='green1')
    sentiment = (Math.random() * (0.2 - 0.0) + 0.0).toFixed(4);
    else if (code=='green2')
    sentiment = (Math.random() * (0.4 - 0.2) + 0.2).toFixed(4);
    else if (code=='green3')
    sentiment = (Math.random() * (0.6 - 0.4) + 0.4).toFixed(4);
    else if (code=='green4')
    sentiment = (Math.random() * (0.8 - 0.6) + 0.6).toFixed(4);
    else if (code=='green5')
    sentiment = (Math.random() * (1 - 0.8) + 0.8).toFixed(4);
    else if (code=='red1')
    sentiment = (Math.random() * (-0.2 - 0.0) - 0.0).toFixed(4);
    else if (code=='red2')
    sentiment = (Math.random() * (-0.4 + 0.2) - 0.2).toFixed(4);
    else if (code=='red3')
    sentiment = (Math.random() * (-0.6 + 0.4) - 0.4).toFixed(4);
    else if (code=='red4')
    sentiment = (Math.random() * (-0.8 + 0.6) - 0.6).toFixed(4);
    else if (code=='red5')
    sentiment = (Math.random() * (-1 + 0.8) - 0.8).toFixed(4);
    else if (code=='defaultFill')
    sentiment = 0;

    return sentiment;

};




var mapData = {};

var random_fill = null;

var map = new Datamap({
scope: 'world',
element: document.getElementById('map'),
responsive:true,
projection: 'mercator',
fills: fills,
data: mapData,
geographyConfig: {
            popupTemplate: function(geo, data) {
                return ['<div class="hoverinfo"><strong>',
                        geo.properties.name,
                        ': ' + code_to_sentiment(data.fillKey),
                        '</strong></div>'].join('');
            }
        }
});




$("#randomiseMap").click(function(){
    // console.log(map);
    for(var i=0;i<countries_codes.length;i++){
    random_fill=fill_codes[Math.floor(Math.random() * fill_codes.length)];
    mapData[countries_codes[i]]={fillKey:random_fill};
}
    map.updateChoropleth(mapData);

});




// $('#randomise').click(function(){
// 	$.get("/v2/analysis/query?month=12&day=6", function(data, status){
//           console.log("Data: " + data.data + "\nStatus: " + status);
//           myPieChart.data.labels = data.labels;
// 		myPieChart.data.datasets[0].data=data.data;
// 		myPieChart.data.datasets[0].backgroundColor = get_colors(data.data.length);
// 		myPieChart.update();

//       });
// });
var selected_categories = [];
var data_for_pie = [];
var filter_form = $('#filter_form');

$('#filter').click(function(){

    $('#exampleModal').modal('toggle');

    setTimeout(function(){
    

        selected_categories = [];
        data_for_pie = [];

        for(var i =2;i<filter_form[0].length;i++){
            if(filter_form[0][i].checked==true){
                // console.log(filter_form[0][i].value)
                selected_categories.push(filter_form[0][i].value);
                data_for_pie.push(random_int(5,20));
            }
        }
        myPieChart.data.labels = selected_categories;
        myPieChart.data.datasets[0].data=data_for_pie;
        myPieChart.data.datasets[0].backgroundColor = get_colors(selected_categories.length);
        myPieChart.update();




        // change pie chart data
          //   $.get("/v2/analysis/query?month=12&day=6", function(data, status){
          //     console.log("Data: " + data.data + "\nStatus: " + status);
          //   myPieChart.data.labels = data.labels;
          //   myPieChart.data.datasets[0].data=data.data;
          //   myPieChart.data.datasets[0].backgroundColor = get_colors(data.data.length);
          //   myPieChart.update();

          // });


        // change map data

        for(var i=0;i<countries_codes.length;i++){
        random_fill=fill_codes[Math.floor(Math.random() * fill_codes.length)];
        mapData[countries_codes[i]]={fillKey:random_fill};
    }
        map.updateChoropleth(mapData);





}, 1000);

});

$('#datepicker-container .input-daterange').datepicker({
    todayHighlight: true
});
