// js file for page: analysis>sentiments


	function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
var arr = [];
var get_data = function(){
arr = [];
for(var i=0;i<5;i++){
arr.push([getRandomInt(-10,10),getRandomInt(-10,10)]);
}
console.log(arr);
return arr;
}

var graph_data = [['score','magnitude']];
// graph_data.push.apply(graph_data,get_data());
var index = null;
var transcript = [];
var transcript_div = $("#transcript");
var status_btn = $("#status_button");
var file_selected = null;
var mouseOverPoint = function(d){
  // console.log(d.index);
  index = d.index;
  // console.log(index);
  transcript_div.html(transcript[index]);
  // console.log(transcript[index]);

};


var chart = c3.generate({
    bindto: '#chart',
    size: {
        height: 600,
        // width: 1000
    },
    data: {
      onmouseover: mouseOverPoint,
      rows: 
        graph_data,
      type: 'spline',
      colors:{
        score:'#00308f',
        magnitude:'#e30074'
    },
    hide: ['magnitude']
    },
    subchart: {
        show: true
    },
    zoom: {
        enabled: true
    },
    axis: {
      y: {
        label: { // ADD
          text: 'Sentiment parameters',
          position: 'outer-middle'
        },
        tick: {
            format: d3.format('.2f')
        }

      },
      x:{
      extent: [0, 5],
      tick:{
      count:10,
      format: d3.format('d')
      }
      }

    },
        regions: [
        {axis: 'y', start: -1, end: -.8, class: 'neg5'},
        {axis: 'y', start: -.8, end: -.6, class: 'neg4'},
        {axis: 'y', start: -.6, end: -.4, class: 'neg3'},
        {axis: 'y', start: -.4, end: -.2, class: 'neg2'},
        {axis: 'y', start: -.2, end: 0, class: 'neg1'},
        {axis: 'y', start: 0,end:1, class: 'pos'},
    ]
//     tooltip: {
//         format: {
//             title: function (d) { return 'Line ' + d; },
//             value: function (value, ratio, id) {
//                 var format = id === 'text' ? d3.format(',') : d3.format('$');
//                 return format(value);
//             }
// //            value: d3.format(',') // apply this format to both y and y2
//         }
//     }
});
$("#update1").click(function(){
chart.load({
  columns: [
    ['sentiments', 30, 200, 100, 400, 150, 250,210,120,110]
  ]
});
});

var arr_to_load = [];
$("#update2").click(function(){
graph_data.push.apply(graph_data,get_data());
chart.flow({
  rows: 
    graph_data,
  length:0
});
});


var setupSocket = function(){
  var socket = io.connect('http://' + document.domain + ':' + location.port);
  console.log("connecting...");
    // socket.on('connect', function() {
    //     socket.emit('my event', {data: 'I\'m connected!'});
    // });

    socket.on('new_data',function(msg){
      // console.log(msg);
      // var arr = [];

      if(msg.type=='analysis_stored'){

      graph_data.push.apply(graph_data,msg.data);
      // console.log(msg.transcript);
      console.log(graph_data);
      transcript.push.apply(transcript,msg.transcript);
      chart.load({
        rows: 
        graph_data,
        length:0
      });
    }
      // result = msg.data
      });

    socket.on('status',function(msg){
  console.log(msg.msg);
  });

        socket.on('process_job',function(msg){
      console.log(msg.call_id);
      console.log(msg.file_name);
  });


  //   socket.on('job_start',function(msg){
  //     // status_btn.html("Processing audio...");
  //     // status_btn.removeClass( "btn-secondary" ).addClass( "btn-warning" );
  // // console.log(msg.msg);
  // });

  //   socket.on('job_done',function(msg){
  // // status_btn.html("Job complete");
  // // status_btn.removeClass( "btn-warning" ).addClass( "btn-success" );
  // });


  // $("#status_button").click(function(){
  //     $.get("/start-job-stored?file="+file_selected, function(data, status){
  //         console.log("Data: " + data.data + "\nStatus: " + status);
  //         status_btn.html("Processing audio...");
  //         status_btn.removeClass( "btn-secondary" ).addClass( "btn-warning" );
  //     });
  // // chart.data([]);
  // // chart.load({unload: true});
  // graph_data = [['score','magnitude']];
  //       chart.load({
  //       rows: 
  //       graph_data,
  //       length:0
  //     });
  // transcript = [];
  // transcript_div.html('transcript goes here');
  // });

  // $("#stop").click(function(){
  //   console.log("disconnecting...");

  //     // $.get("/disconnect-socket");
  //   socket.disconnect();
  // });

};

// filter search logic
var filter = $("#filter");
var filter_search_list = $("#filter_search_list");
var filter_search_form = $("#filter_search_form");
var filter_search_month = $("#filter_search_month");
var filter_search_day = $("#filter_search_day");
var calls_made_h6 = $("#calls_made_h6");

var files_actual = ['pos_final','ang_final','recording4','recording5','recording7'];
var files_to_show = ['file_001','file_002','file_005','file_006','file_007'];

var sentiments = [0.67,-0.4,0.4,-0.2,0.5];
var span_html = null;
var new_el = null;
var span_type = null;
var ind = null;
filter.click(function(){
  // console.log(filter_search_month);
  $('#exampleModal').modal('toggle');
      // $.get('/filter-search?month='+filter_search_month[0].value+'&day='+filter_search_day[0].value, function(data, status){
      //   var file_name;
      //   var new_el;
      //   filter_search_list.empty();
      //   if (data.data.files.length>0){
      //     calls_made_h6.html("Calls for this date:");
      //   for (var i=0;i<data.data.files.length;i++)
      //   {
      //       // console.log(data.data.files[i])
      //       new_el = '<a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">'+data.data.files[i]+'</a>'
      //   filter_search_list.append(new_el);
          
      //   }
      // }
      // else{
      //   calls_made_h6.html("No calls found for this date");
      //   // filter_search_list.append('<a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">No files found for this date</a>');
      // }
      //     // console.log(data);
      //     // status_btn.html("Processing audio...");
      //     // status_btn.removeClass( "btn-secondary" ).addClass( "btn-warning" );
      // }); 
      filter_search_list.empty();

      calls_made_h6.html("Calls for this date:");
        for (var i=0;i<files_to_show.length;i++)
        {
            // console.log(data.data.files[i])
            if(sentiments[i]>0.3){
              span_type = 'success'
            }
            else if(sentiments[i]<-0.3){
              span_type = 'danger'
            }
            else {
              span_type = 'warning'
            }
        new_el = '<a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">'+files_to_show[i]+'&nbsp;&nbsp;&nbsp;<span class="badge badge-'+span_type+' badge-pill">'+sentiments[i]+'</span>'+'</a>'
        filter_search_list.append(new_el);  
        }    


});

filter_search_list.on('click','a',function(){
  ind = files_to_show.indexOf(this.text.split('   ')[0]);
  // file_selected = this.text.split(' ')[0];
  file_selected = files_actual[ind];

      $.get("/start-job-stored?file="+file_selected, function(data, status){
          console.log("Data: " + data.data + "\nStatus: " + status);
          // status_btn.html("Processing audio...");
          // status_btn.removeClass( "btn-secondary" ).addClass( "btn-warning" );
      });
  // chart.data([]);
  // chart.load({unload: true});
  graph_data = [['score','magnitude']];
        chart.load({
        rows: 
        graph_data,
        length:0
      });
  transcript = [];
  transcript_div.html('transcript goes here');


  
  // console.log(this);
  // status_btn.html("View sentiments");
  // status_btn.removeClass( "btn-warning" ).removeClass( "btn-success" ).addClass('btn-secondary');
  // console.log(file_selected);
});

$( document ).ready(function() {
  setupSocket();

});


$('#datepicker-container .input-daterange').datepicker({
    todayHighlight: true
});

