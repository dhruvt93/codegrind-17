var random_int = function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var get_colors = function(length){
	var arr = [];
	for (var i=0;i<length;i++){
		arr.push(dynamicColors());
	}
	return arr;
}

var dynamicColors = function() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
}

var get_random_sentiment = function(){
    return ((Math.random() * (2)) -1).toFixed(2);
}

var ctx = document.getElementById("myChart");


data = {
    datasets: [{
        data: [10],
        backgroundColor:[]
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: ['placeholder']
};

options = {'legend':
                {'position':'bottom'}
            }
var myPieChart = new Chart(ctx,{
    type: 'doughnut',
    data: data,
    options: options
});


var ctx2 = document.getElementById("chart_sent_per_cat");

// var barChartOptions = {'legend':
//                 {'position':'bottom'}
//             }
var barChartData = {
            labels: [],
            datasets: [{
                label:'Categories',
                data: [],
                backgroundColor:[]
            }]
        };


var myBarChart = new Chart(ctx2, {
    type: 'bar',
    data: barChartData
    // options: barChartOptions
});



var selected_categories = [];
var data_for_pie = [];
var filter_form = $('#filter_form');
var colors =[];
var data_for_bar = [];

$('#filter').click(function(){

    $('#exampleModal').modal('toggle');

    setTimeout(function(){
    

        selected_categories = [];
        data_for_pie = [];
        data_for_bar = [];

        for(var i =2;i<filter_form[0].length;i++){
            if(filter_form[0][i].checked==true){
                // console.log(filter_form[0][i].value)
                selected_categories.push(filter_form[0][i].value);
                data_for_pie.push(random_int(5,20));
                data_for_bar.push(get_random_sentiment());

            }
        }
        colors = get_colors(selected_categories.length)
        // update pie chart
        myPieChart.data.labels = selected_categories;
        myPieChart.data.datasets[0].data=data_for_pie;
        myPieChart.data.datasets[0].backgroundColor = colors;
        myPieChart.update();


        // update bar chart
        myBarChart.data.labels = selected_categories;
        myBarChart.data.datasets[0].data=data_for_bar;
        myBarChart.data.datasets[0].backgroundColor = colors;
        myBarChart.update();


}, 1000);

});

$('#datepicker-container .input-daterange').datepicker({
    todayHighlight: true
});
