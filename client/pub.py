from google.cloud import pubsub_v1
import time, json

def publish_messages(project, topic_name):
    """Publishes multiple messages to a Pub/Sub topic."""
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project, topic_name)

    for i in range(10):
        data = {}
        # filename = raw_input('Filename?\n')
        file_name = 'new_file_'+str(i)+'.flac'
        data['file_name'] = file_name
        # data['call_id'] = str(143431)
        # data['callerID'] = '1234'
        # data['time'] = int(time.time())
        # Data must be a bytestring
        #data = data.encode('utf-8')
        publisher.publish(topic_path, data=json.dumps(data))
        print data
        time.sleep(10)

    print('Published messages.')

if __name__ == '__main__':
    publish_messages("helloworld-166506","codegrind-rt")