from flask import Flask, render_template,jsonify
from flask_socketio import SocketIO
from flask_socketio import send, emit, disconnect
import random
import json
from time import sleep
from celery import Celery


async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

socketio = SocketIO(app)

# def background_thread():
#     counter = 1
#     for i in range(5):
#         arr = []
#         mul=5
#         start = counter*mul
#         end = start+mul
#         for i in range(start,end):
#             arr.append({"x":i+1,"y":round(random.random(),2),"y0":0,"meta":{"key":"first"}})
#         counter+=1
#         socketio.emit('new_data',
#                       {'data': json.dumps(arr)})
#         # time.sleep(3)

#     socketio.emit('disconnect_req',
#          {'msg': 'Job done! Send disconnect request'})

def secondary(msg):
    print("hehe")
    socketio.emit('new_data',
         {'msg': 'Job done! Send disconnect request'})

@celery.task
def dummy_task(msg):
    for i in range(5):
        # print(msg)
        secondary(msg)
        sleep(5)

# @app.route("/")
# def index():
#     dummy_task.apply_async(args=["haha"])
#     return render_template("basic.html")



@socketio.on('connect')
def test_connect():
    print("connected")
    # emit('my_response', {'data': 'Connected', 'count': 0})

@app.route("/")
def index():
    dummy_task.apply_async(args=["haha"])
    return render_template("lines_sock2.html")

@app.route("/refresh/<int:counter>")
def refresh(counter):
    arr = []
    mul=5
    start = counter*mul
    end = start+mul
    for i in range(start,end):
        arr.append({"x":i+1,"y":round(random.random(),2),"y0":0,"meta":{"key":"first"}})
    return jsonify(arr)

@app.route("/v2")
def v2():
    return render_template("c3_demo.html")

    
if __name__ == '__main__':
    socketio.run(app,use_reloader=True)