
# coding: utf-8

from google.cloud import speech


from google.cloud import language


import pprint


from math import ceil


import pickle

import argparse


class AudioProcessor:
    def __init__(self):
        self.speech_client = speech.SpeechClient()
        self.lang_client = language.LanguageServiceClient()
        self.x_counter = 0
        
    def get_audio_to_text_operation(self,uri):
        audio=speech.types.RecognitionAudio(
                uri=uri
            )

        config=speech.types.RecognitionConfig(
                encoding='FLAC',
                language_code='en-IN'
            )
        operation = self.speech_client.long_running_recognize(config, audio)
        return operation
    
    def get_transcript_from_file_uri(self,uri):
#         bucket_address = "gs://samples-112/new_samples/converted/"
#         uri = bucket_address+uri
        operation = self.get_audio_to_text_operation(uri)
    
        """
        results {
        alternatives {
        transcript: "specification the meeting this place I\'d rather be more affordable so you and make your career success"
        confidence: 0.9146217703819275
        }
        }
        results {
        alternatives {
        transcript: " I am"
        confidence: 0.4852932095527649
        }
        }
        results {
        alternatives {
        transcript: " useful apps are giving employee discount of 40% right so my supervisor another 40% suggest that pay you $80 in INR 6ad what your DP is only $16.76 every month for everybody how about that"
        confidence: 0.872408926486969
        }
        }
        results {
        alternatives {
        transcript: " Darshan craze how can I that it\'s over I\'m expecting your a pretrial until August 21st August 24th you will be charged only $16.76 ok so everythings ABCD"
        confidence: 0.9013625383377075
        }
        }
        """

        response = operation.result(timeout=200)
        transcript = []
        for result in response.results:
            transcript.append(result.alternatives[0].transcript)
        return transcript
    
    def analyze_text(self,transcript):
        document = language.types.Document(
            content=transcript,
            language='en',
            type='PLAIN_TEXT',
        )

        response = self.lang_client.analyze_sentiment(
            document=document,
            encoding_type='UTF32',
        )
        
        """
        
        sentences {
        text {
        content: "specification the meeting this place I\'d rather be more affordable so you and make your career success."
        }
        sentiment {
        magnitude: 0.699999988079071
        score: 0.699999988079071
        }
        }
        sentences {
        text {
        content: "I am"
        begin_offset: 105
        }
        sentiment {
        magnitude: 0.10000000149011612
        score: 0.10000000149011612
        }
        }
        document_sentiment {
        magnitude: 0.800000011920929
        score: 0.4000000059604645
        }
        language: "en"
        
        """
        
        data = [[],[]]
        for el in response.sentences:
            data[0].append([el.sentiment.score,el.sentiment.magnitude])
            data[1].append([el.text.content])
            self.x_counter+=1
        return data
        


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--file_name')
    args = parser.parse_args()

    file_name=args.file_name
    
    call_id = ''
    counter = ''
    # file = 'new_file_0.flac'
    bucket_uri = 'streaming-upload-audio'
    file_uri = 'gs://'+bucket_uri+'/'+file_name
    processor_obj = AudioProcessor()
    transcript = processor_obj.get_transcript_from_file_uri(file_uri)
    print("got transcript")
    text = ". ".join(transcript)
    response = processor_obj.analyze_text(text)
    # pprint.pprint(response)
    # print(type(response))

    
#     socketio stuff
    # data = {'call_id':call_id,'counter':counter,'sentiments':response[0],'transcripts':response[1]}
    # socketio.emit('new_data',data)

    with open('pickled/'+file_name.split('.')[0]+'.pickle', 'wb') as f:
        pickle.dump(response, f, pickle.HIGHEST_PROTOCOL)
    print("wrote to file")


# get_ipython().system('jupyter nbconvert --to python rt_pipeline.ipynb --output ../client/rt_pipeline.py --no-prompt')




