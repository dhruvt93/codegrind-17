# from bokeh.plotting import figure
# from bokeh.embed import components
from flask import Flask, render_template,jsonify
# import json
from random import randint
import random

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("lines_sock2_static.html")

@app.route("/lines")
def lines():
    return render_template("lines.html")


@app.route("/refresh/<int:counter>")
def refresh(counter):
	arr = []
	mul=5
	start = counter*mul
	end = start+mul
	for i in range(start,end):
		arr.append({"x":i+1,"y":round(random.random(),2),"y0":0,"meta":{"key":"first"}})
	return jsonify(arr)

    
if __name__ == '__main__':
    app.run(debug=True)