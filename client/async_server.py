from flask import Flask, render_template,jsonify
# import json
from random import randint
import random
from celery import Celery
from time import sleep

app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


@celery.task
def dummy_task(msg):
    for i in range(5):
        print(msg)
        sleep(5)

@app.route("/")
def index():
    dummy_task.apply_async(args=["haha"])
    return render_template("basic.html")
    
if __name__ == '__main__':
    app.run(debug=True)