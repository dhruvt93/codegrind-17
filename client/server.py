from flask import Flask, render_template,jsonify,request
from flask_socketio import SocketIO
from flask_socketio import send, emit, disconnect
import random
import json
from time import sleep
from celery import Celery
import eventlet
import pickle

eventlet.monkey_patch()

async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'
app.config['TEMPLATES_AUTO_RELOAD'] = True

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
REDIS_URL = 'redis://'
# app.config['MESSAGE_QUEUE'] = REDIS_URL

celery.conf.update(app.config)

socketio = SocketIO(app,message_queue=REDIS_URL)

@celery.task
def dummy_task(msg):
    for i in range(5):
        print(msg)
        socketio.emit('status',{'msg':'Starting audio processing'})
        sleep(5)

@celery.task
def audio_processing():
    socketio.emit('status',{'msg':'Starting audio processing'})
    responses = []
    files = ['cust_trimmed_000.pickle',
             'cust_trimmed_001.pickle',
             'cust_trimmed_002.pickle',
             'cust_trimmed_003.pickle',
             'cust_trimmed_004.pickle',
            ]
    for file in files:
        with open(file, 'rb') as f:
            response = pickle.load(f)
            responses.append(response)
    # pprint.pprint(response)
    for response in responses:
        socketio.emit('new_data',{'data':response})
        sleep(1)

    socketio.emit('job_done')

@socketio.on('connect')
def test_connect():
    print("connected")
    socketio.emit('status',{'msg':'Connected yo!'})
    # emit('my_response', {'data': 'Connected', 'count': 0})

@app.route("/")
def index():
    return render_template("graph.html")

@app.route("/mock")
def mock():
    return render_template("lines_sock2_static.html")


# handler for real-time full file
@app.route("/start-job")
def start_job():
    # file = request.args.get('file')
    files = None
    base_addr = None
    # if file == 'inbound':
    #     files = ['inbound_trimmed_000.pickle',
    #             'inbound_trimmed_001.pickle',
    #             'inbound_trimmed_002.pickle',
    #             'inbound_trimmed_003.pickle',
    #             # 'inbound_trimmed_004.pickle',
    #             # 'inbound_trimmed_005.pickle',
    #             # 'inbound_trimmed_006.pickle',
    #             # 'inbound_trimmed_007.pickle',
    #             # 'inbound_trimmed_008.pickle',
    #             # 'inbound_trimmed_009.pickle',
    #             # 'inbound_trimmed_010.pickle',
    #             # 'inbound_trimmed_011.pickle',
    #             ]
    #     base_addr = 'data/v2/inbound/'
    # elif file == 'ang_final':
    #     files = ['ang_final_0.pickle',
    #             'ang_final_1.pickle',
    #             'ang_final_2.pickle',
    #             'ang_final_3.pickle',
    #             'ang_final_4.pickle',
    #             'ang_final_5.pickle',
    #             ]
    #     base_addr = 'data/v2/ang_final/'
    # elif file == 'pos_final':
    #     files = ['pos_final.pickle',
    #             ]
    #     # files = ['pos_final_0.pickle',
    #     #         'pos_final_1.pickle',
    #     #         'pos_final_2.pickle',
    #     #         'pos_final_3.pickle',
    #     #         'pos_final_4.pickle'
    #     #         ]
    #     base_addr = 'data/v2/pos_final1/'
    # elif file == 'recording4':
    #     files = ['recording4.pickle'
    #             ]
    #     base_addr = 'data/v2/other/'
    # elif file == 'recording5':
    #     files = ['recording5.pickle'
    #             ]
    #     base_addr = 'data/v2/other/'
    # elif file == 'recording7':
    #     files = ['recording7.pickle'
    #             ]
    #     base_addr = 'data/v2/other/'
    # # dummy_task.apply_async(args=["haha"])
    files = ['ang_final_0.pickle',
            'ang_final_1.pickle',
            'ang_final_2.pickle',
            'ang_final_3.pickle',
            'ang_final_4.pickle',
            'ang_final_5.pickle',
            'ang_final_6.pickle',
            'ang_final_7.pickle',
            'ang_final_8.pickle',
            ]
    base_addr = 'data/v2/ang_final2/'
    if not (files==None and base_addr==None):
        audio_processing_v2.apply_async(args=[files,base_addr])
        return jsonify({"data":"Job started yo!"})
    else:
        return jsonify({"data":"Job not started. File parameter incorrect."})

# @app.route("/disconnect-socket")
# def disconnect_socket():
#     disconnect()
#     return jsonify({})


# celery task for real-time full file
@celery.task
def audio_processing_v2(files,base_addr):
    socketio.emit('job_start',{'msg':'Starting audio processing'})
    responses = []
    transcripts = []
    for file in files:
        with open(base_addr+file, 'rb') as f:
            response = pickle.load(f)
            responses.append(response[0])
            transcripts.append(response[1])
    # pprint.pprint(response)
    # with open('sample_transcript.pickle', 'rb') as f:
    #     transcript = pickle.load(f)
    # sleep(4)
    for i,response in enumerate(responses):
        socketio.emit('new_data',{'type':'real_time_file','data':response,'transcript':transcripts[i][:len(response)]})
        sleep(6)
    socketio.emit('job_done')



@app.route("/start-job-stored")
def start_job_stored():
    file = request.args.get('file')
    files = None
    base_addr = None
    if file == 'ang_final':
        files = ['ang_final.pickle',
                ]
        base_addr = 'data/v2/ang_final1/'
    elif file == 'pos_final':
        files = ['pos_final.pickle',
                ]
        # files = ['pos_final_0.pickle',
        #         'pos_final_1.pickle',
        #         'pos_final_2.pickle',
        #         'pos_final_3.pickle',
        #         'pos_final_4.pickle'
        #         ]
        base_addr = 'data/v2/pos_final1/'
    elif file == 'recording4':
        files = ['recording4.pickle'
                ]
        base_addr = 'data/v2/other/'
    elif file == 'recording5':
        files = ['recording5.pickle'
                ]
        base_addr = 'data/v2/other/'
    elif file == 'recording7':
        files = ['recording7.pickle'
                ]
        base_addr = 'data/v2/other/'
    # dummy_task.apply_async(args=["haha"])
    if not (files==None and base_addr==None):
        audio_processing_v2_stored.apply_async(args=[files,base_addr])
        return jsonify({"data":"Job started yo!"})
    else:
        return jsonify({"data":"Job not started. File parameter incorrect."})

# @app.route("/disconnect-socket")
# def disconnect_socket():
#     disconnect()
#     return jsonify({})

@celery.task
def audio_processing_v2_stored(files,base_addr):
    socketio.emit('job_start',{'msg':'Starting audio processing'})
    responses = []
    transcripts = []
    for file in files:
        with open(base_addr+file, 'rb') as f:
            response = pickle.load(f)
            responses.append(response[0])
            transcripts.append(response[1])
    # pprint.pprint(response)
    # with open('sample_transcript.pickle', 'rb') as f:
    #     transcript = pickle.load(f)
    # sleep(4)
    for i,response in enumerate(responses):
        socketio.emit('new_data',{'type':'analysis_stored','data':response,'transcript':transcripts[i][:len(response)]})
        sleep(4)
    socketio.emit('job_done')





@celery.task
def fetch_data_task(file_name):
    socketio.emit('status',{'msg':'Fetching data'})
    base_addr = 'pickled/'
    file_name += ".pickle"
    with open(base_addr+file_name, 'rb') as f:
        pickle_data = pickle.load(f)
        response = pickle_data[0]
        transcript = pickle_data[1]
    socketio.emit('new_data',{'type':'real_time_stream','data':response,'transcript':transcript})

@celery.task
def fetch_data_task2():
    socketio.emit('status',{'msg':'Fetching data'})
    # base_addr = 'pickled/'
    file = 'data/v2/pos_final1/pos_final.pickle'
    with open(file, 'rb') as f:
        pickle_data = pickle.load(f)
    response = pickle_data[0]
    transcript = pickle_data[1]
    for i in range(len(transcript)):
        socketio.emit('new_data',{'type':'real_time_stream','data':[response[i]],'transcript':[transcript[i]]})
        sleep(8)

@app.route("/fetch-data")
def fetch_data():
    file_name = request.args.get('file_name')
    fetch_data_task.apply_async(args=[file_name])
    return "200,OK"

@app.route("/v2/real-time")
def real_time():
    return render_template("rt_stream.html",active="real_time")

@app.route("/v2/real-time-from-file")
def real_time_from_file():
    return render_template("rt_file.html",active="real_time",button_title="Waiting for remote...")


@app.route("/v2")
def v2():
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    days = list(range(1,32))
    labels = ['card','loan','aadhar','SIP','bank_statements',\
            'transfers','time_deposits','cheque_enquiry',\
            'rewards_programs','phone_banking','pin_update',\
            'rate_enquiry','internet_banking','remittance',\
            'card_benefits','cheque','dropbox']
    service_agents = ['agent001','agent002','agent003','agent004','agent005']
    return render_template("dashboard.html",button_title="Select call",months=months,days=days,categories=labels,active='sentiment',service_agents=service_agents)

@app.route("/v2/remote")
def v2_remote():
    # file = request.args.get('file')
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']

    return render_template("dashboard.html",button_title="Waiting for remote...",months=months)

@app.route("/filter-search")
def filter_search():
    month = request.args.get('month')
    day = request.args.get('day')
    files = []
    if (month=='December' and day=='6') or (month=='January' and day=='1'):
        files = ['pos_final','ang_final','inbound']
    return jsonify({'data':{'files':files}})

@app.route("/v2/analysis")
def analysis():
    # file = request.args.get('file')
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    days = list(range(1,32))
    labels = ['card','loan','aadhar','SIP','bank_statements',\
            'transfers','time_deposits','cheque_enquiry',\
            'rewards_programs','phone_banking','pin_update',\
            'rate_enquiry','internet_banking','remittance',\
            'card_benefits','cheque','dropbox']
    return render_template('analysis.html',months=months,days=days,categories=labels,active='analysis')

@app.route("/v2/analysis-cat")
def analysis_cat():
    # file = request.args.get('file')
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    days = list(range(1,32))
    labels = ['card','loan','aadhar','SIP','bank_statements',\
            'transfers','time_deposits','cheque_enquiry',\
            'rewards_programs','phone_banking','pin_update',\
            'rate_enquiry','internet_banking','remittance',\
            'card_benefits','cheque','dropbox']
    return render_template('analysis_cat.html',months=months,days=days,categories=labels,active='analysis_cat')


@app.route("/v2/analysis-geo")
def analysis_geo():
    # file = request.args.get('file')
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    days = list(range(1,32))
    labels = ['card','loan','aadhar','SIP','bank_statements',\
            'transfers','time_deposits','cheque_enquiry',\
            'rewards_programs','phone_banking','pin_update',\
            'rate_enquiry','internet_banking','remittance',\
            'card_benefits','cheque','dropbox']
    return render_template('analysis_geo.html',months=months,days=days,categories=labels,active='analysis_geo')




@app.route("/v2/analysis/query")
def analysis_query():
    month = request.args.get('month')
    day = request.args.get('day')
    labels = ['card','loan','aadhar','SIP','bank_statements',\
                'transfers','time_deposits','cheque_enquiry',\
                'rewards_programs','phone_banking','pin_update',\
                'rate_enquiry','internet_banking','remittance',\
                'card_benefits','cheque','dropbox']
    data = []
    labels_selected = []
    num = random.randint(3,len(labels))
    ids = random.sample(range(0, len(labels)), num)

    for id in ids:
        labels_selected.append(labels[id])
        data.append(random.randint(5,30))


    return jsonify({'status':'200','data':data,'labels':labels_selected})


@app.route("/info")
def info_page():
    return render_template('info.html')

if __name__ == '__main__':
    socketio.run(app,use_reloader=True,debug=True)