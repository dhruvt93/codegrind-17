from flask import Flask, render_template,jsonify,request
from flask_socketio import SocketIO
from flask_socketio import send, emit, disconnect
import random
import json
from time import sleep
from celery import Celery
import eventlet
import pickle
from google.cloud import pubsub_v1
from rt_pipeline import AudioProcessor

eventlet.monkey_patch()

async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'top-secret!'
app.config['TEMPLATES_AUTO_RELOAD'] = True

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
REDIS_URL = 'redis://'
# app.config['MESSAGE_QUEUE'] = REDIS_URL

celery.conf.update(app.config)

socketio = SocketIO(app,message_queue=REDIS_URL)

def callback(message):
    print('Received message: {}'.format(message))
    message.ack()

# @celery.task
# def pub_sub_listener():
#     print('starting pub sub listener')
#     # socketio.emit('status',{'msg':'Starting pub sub listener'})
#     project = "helloworld-166506"
#     subscription_name = "codegrind-dhruv"
#     subscriber = pubsub_v1.SubscriberClient()
#     subscription_path = subscriber.subscription_path(
#         project, subscription_name)

#     subscriber.subscribe(subscription_path, callback=callback)

#     receive_messages("helloworld-166506","codegrind-dhruv")

#     while True:
#         print('sleeping...')
#         sleep(5)



@celery.task
def dummy_task(msg):
    for i in range(5):
        print(msg)
        socketio.emit('status',{'msg':'Starting audio processing'})
        sleep(5)

@celery.task
def audio_processing():
    socketio.emit('status',{'msg':'Starting audio processing'})
    responses = []
    files = ['cust_trimmed_000.pickle',
             'cust_trimmed_001.pickle',
             'cust_trimmed_002.pickle',
             'cust_trimmed_003.pickle',
             'cust_trimmed_004.pickle',
            ]
    for file in files:
        with open(file, 'rb') as f:
            response = pickle.load(f)
            responses.append(response)
    # pprint.pprint(response)
    for response in responses:
        socketio.emit('new_data',{'data':response})
        sleep(1)

    socketio.emit('job_done')

@socketio.on('connect')
def test_connect():
    print("connected")
    socketio.emit('status',{'msg':'Connected yo!'})
    # emit('my_response', {'data': 'Connected', 'count': 0})

@app.route("/")
def index():
    return render_template("graph.html")

@app.route("/mock")
def mock():
    return render_template("lines_sock2_static.html")

@app.route("/start-job")
def start_job():
    file = request.args.get('file')
    files = None
    base_addr = None
    if file == 'inbound':
        files = ['inbound_trimmed_000.pickle',
                'inbound_trimmed_001.pickle',
                'inbound_trimmed_002.pickle',
                'inbound_trimmed_003.pickle',
                # 'inbound_trimmed_004.pickle',
                # 'inbound_trimmed_005.pickle',
                # 'inbound_trimmed_006.pickle',
                # 'inbound_trimmed_007.pickle',
                # 'inbound_trimmed_008.pickle',
                # 'inbound_trimmed_009.pickle',
                # 'inbound_trimmed_010.pickle',
                # 'inbound_trimmed_011.pickle',
                ]
        base_addr = 'data/v2/inbound/'
    elif file == 'ang_final':
        files = ['ang_final_0.pickle',
                'ang_final_1.pickle',
                'ang_final_2.pickle',
                'ang_final_3.pickle',
                'ang_final_4.pickle',
                'ang_final_5.pickle',
                ]
        base_addr = 'data/v2/ang_final/'
    elif file == 'pos_final':
        files = ['pos_final_0.pickle',
                'pos_final_1.pickle',
                'pos_final_2.pickle',
                'pos_final_3.pickle',
                'pos_final_4.pickle'
                ]
        base_addr = 'data/v2/pos_final/'
    # dummy_task.apply_async(args=["haha"])
    if not (files==None and base_addr==None):
        audio_processing_v2.apply_async(args=[files,base_addr])
        return jsonify({"data":"Job started yo!"})
    else:
        return jsonify({"data":"Job not started. File parameter incorrect."})

# @app.route("/disconnect-socket")
# def disconnect_socket():
#     disconnect()
#     return jsonify({})

@celery.task
def process_file(call_id,file_name,time_limit=5):
    # sleep(3)
    # d = {'call_id':str(call_id),'file_name':file_name}
    # print(json.dumps(d))
    # socketio.emit('process_job',d)
    socketio.emit('status',{'msg':'Starting processing on file'})
    sleep(10)
#     bucket_uri = 'streaming-upload-audio'
#     file = 'new_file_0.flac'

#     file_uri = 'gs://'+bucket_uri+'/'+file
#     processor_obj = AudioProcessor()
#     print(processor_obj)
#     transcript = processor_obj.get_transcript_from_file_uri(file_uri)
#     print("got transcript of length-> "+str(len(transcript)))
#     text = ". ".join(transcript)
#     response = processor_obj.analyze_text(text)
    
# #     socketio stuff
#     data = {'call_id':call_id,'counter':counter,'sentiments':response[0],'transcripts':response[1]}
#     socketio.emit('new_data',data)

@app.route("/start-process")
def start_process():
    file_name = request.args.get('file_name')
    call_id = request.args.get('call_id')
    process_file.apply_async(args=[call_id,file_name])
    return "200,OK"


@celery.task
def audio_processing_v2(files,base_addr):
    socketio.emit('job_start',{'msg':'Starting audio processing'})
    responses = []
    transcripts = []

    for file in files:
        with open(base_addr+file, 'rb') as f:
            response = pickle.load(f)
            responses.append(response[0])
            transcripts.append(response[1])
    # pprint.pprint(response)
    # with open('sample_transcript.pickle', 'rb') as f:
    #     transcript = pickle.load(f)
    # sleep(4)
    for i,response in enumerate(responses):
        socketio.emit('new_data',{'data':response,'transcript':transcripts[i][:len(response)]})
        sleep(1)
    socketio.emit('job_done')


# @app.route("/start-pub-sub")
# def start_pub_sub():
#     pub_sub_listener.apply_async(args=None)
#     return "200"


@app.route("/v2")
def v2():
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    days = list(range(1,32))
    labels = ['card','loan','aadhar','SIP','bank_statements',\
            'transfers','time_deposits','cheque_enquiry',\
            'rewards_programs','phone_banking','pin_update',\
            'rate_enquiry','internet_banking','remittance',\
            'card_benefits','cheque','dropbox']
    return render_template("dashboard.html",button_title="Select call",months=months,days=days,categories=labels,active='sentiment')

@app.route("/v2/remote")
def v2_remote():
    # file = request.args.get('file')
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']

    return render_template("dashboard.html",button_title="Waiting for remote...",months=months)

@app.route("/filter-search")
def filter_search():
    month = request.args.get('month')
    day = request.args.get('day')
    files = []
    if (month=='December' and day=='6') or (month=='January' and day=='1'):
        files = ['pos_final','ang_final','inbound']
    return jsonify({'data':{'files':files}})

@app.route("/v2/analysis")
def analysis():
    # file = request.args.get('file')
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    days = list(range(1,32))
    labels = ['card','loan','aadhar','SIP','bank_statements',\
            'transfers','time_deposits','cheque_enquiry',\
            'rewards_programs','phone_banking','pin_update',\
            'rate_enquiry','internet_banking','remittance',\
            'card_benefits','cheque','dropbox']
    return render_template('analysis.html',months=months,days=days,categories=labels,active='analysis')

@app.route("/v2/analysis-cat")
def analysis_cat():
    # file = request.args.get('file')
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    days = list(range(1,32))
    labels = ['card','loan','aadhar','SIP','bank_statements',\
            'transfers','time_deposits','cheque_enquiry',\
            'rewards_programs','phone_banking','pin_update',\
            'rate_enquiry','internet_banking','remittance',\
            'card_benefits','cheque','dropbox']
    return render_template('analysis_cat.html',months=months,days=days,categories=labels,active='analysis_cat')


@app.route("/v2/analysis-geo")
def analysis_geo():
    # file = request.args.get('file')
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    days = list(range(1,32))
    labels = ['card','loan','aadhar','SIP','bank_statements',\
            'transfers','time_deposits','cheque_enquiry',\
            'rewards_programs','phone_banking','pin_update',\
            'rate_enquiry','internet_banking','remittance',\
            'card_benefits','cheque','dropbox']
    return render_template('analysis_geo.html',months=months,days=days,categories=labels,active='analysis_geo')




@app.route("/v2/analysis/query")
def analysis_query():
    month = request.args.get('month')
    day = request.args.get('day')
    labels = ['card','loan','aadhar','SIP','bank_statements',\
                'transfers','time_deposits','cheque_enquiry',\
                'rewards_programs','phone_banking','pin_update',\
                'rate_enquiry','internet_banking','remittance',\
                'card_benefits','cheque','dropbox']
    data = []
    labels_selected = []
    num = random.randint(3,len(labels))
    ids = random.sample(range(0, len(labels)), num)

    for id in ids:
        labels_selected.append(labels[id])
        data.append(random.randint(5,30))
    return jsonify({'status':'200','data':data,'labels':labels_selected})


if __name__ == '__main__':
    # pub_sub_listener.apply_async(args=None)
    socketio.run(app,use_reloader=True,debug=True)