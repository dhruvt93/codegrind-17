
# coding: utf-8

from google.cloud import speech


from google.cloud import language


import pprint


from math import ceil


import pickle


class AudioProcessor:
    def __init__(self):
        self.speech_client = speech.SpeechClient()
        self.lang_client = language.LanguageServiceClient()
        self.x_counter = 0
        
    def get_audio_to_text_operation(self,uri):
        audio=speech.types.RecognitionAudio(
                uri=uri
            )

        config=speech.types.RecognitionConfig(
                encoding='FLAC',
                language_code='en-US'
            )
        operation = self.speech_client.long_running_recognize(config, audio)
        return operation
    
    def get_transcript_from_file_uri(self,uri):
        bucket_address = "gs://samples-112/samples/samp2/"
        uri = bucket_address+uri
        operation = self.get_audio_to_text_operation(uri)
        response = operation.result(timeout=90)
        transcript = []
        for result in response.results:
            transcript.append(result.alternatives[0].transcript)
        return transcript
    
    def analyze_text(self,transcript):
        document = language.types.Document(
            content=transcript,
            language='en',
            type='PLAIN_TEXT',
        )

        response = self.lang_client.analyze_sentiment(
            document=document,
            encoding_type='UTF32',
        )
        data = []
        for el in response.sentences:
            data.append({'x':self.x_counter,'y':el.sentiment.score,'y0':0,                         'meta':{'text':el.text.content,'magnitude':el.sentiment.magnitude}})
            self.x_counter+=1
#             data.append({'text':el.text.content,'sentiment':{'magnitude':el.sentiment.magnitude,'score':el.sentiment.score}})
        return data
        
        


if __name__=="__main__":
    
#     with open('../client/data.pickle', 'rb') as f:
#         data = pickle.load(f)
        
    file_uri_list = ['cust_trimmed_000.flac',
                     'cust_trimmed_001.flac',
                     'cust_trimmed_002.flac',
                     'cust_trimmed_003.flac',
                     'cust_trimmed_004.flac',
                    ]
    processor_obj = AudioProcessor()
    for file in file_uri_list:
        transcript = processor_obj.get_transcript_from_file_uri(file)
        print("got transcript")
        num_lines = len(transcript)
        one_time = 30
        divides = ceil(num_lines/one_time)
        portion = num_lines/divides
        sentiment_data = {}
        for i in range(divides):
            start = ceil(i*portion)
            end = ceil(start+portion)
#             print(start,end)
            text = "".join(transcript[start:end])
            response = processor_obj.analyze_text(text)
            pprint.pprint(response)
            
            with open('../client/'+file.split('.')[0]+'.pickle', 'wb') as f:
                pickle.dump(response, f, pickle.HIGHEST_PROTOCOL)

#             emit('new_data',{'data':response})


# get_ipython().system('jupyter nbconvert --to python background.ipynb --output ../client/audioprocessor.py --no-prompt')




